# TruliDist 

A PHP backend serving JSON to a React frontend.

**username**: user **password**: user

**Features**

- [x] Login
- [x] Logout
- [x] Sign-up
- [] User management
- [x] User rights management (partial)
- [x] Document uploads
- [] Document access rights
- [x] Document modification
- [x] Document deletion
- [x] Document encryption
- [x] Document download and/or display
- [] Security thread management

**Untested for the following conditions**

- Upload of large files


## Folder structure
```
public_html
├── app
├── docs
├── server
└── .htaccess
```

## Api endpoints

### User

**Definition**

`POST /login`

**Arguments**

- `"username":string`
- `"password":string`

**Response**

- `200 OK` on success

```json
{
    "id": "6",
    "username": "user",
    "allow_edit": "1"
}
```

### Register

**Definition**

`POST /register`

**Arguments**

- `"username":string`
- `"password":string`

**Response**

- `200 OK` on success

```json
{
    "id": "6",
    "username": "user",
    "allow_edit": "1",
    "token": "afOFyADGkaeplfbaLgPWdaOTwpba"
}
```

### Lookup user details

`GET /user`  Authorization required

**Response**

- `404 Not found` if user does not exist
- `200 OK` on success

```json
{
    "id": "6",
    "username": "user",
    "allow_edit": "1"
}
```


### Lookup files

`GET /files` Authorization required

**Response**

- `200 OK` on success

```json
[
    {
        "id": "33",
        "name": "file.png",
        "mime": "image/png",
        "date_created": "2019-08-16 10:30:30",
        "date_modified": "2019-08-16 10:31:04"
    },
    {
        "id": "32",
        "name": "file(2).png",
        "mime": "image/png",
        "date_created": "2019-08-16 10:21:42",
        "date_modified": null
    },
]
```

### Create file

`POST /file` Authorization required

**Response**

- `200 OK` on success

```json
{
    "id": "10",
    "user_id": "6",
    "name": "file.svg",
    "mime": "image/svg+xml",
    "date_created": "2019-08-15 17:58:36",
    "date_modified": null
}
```

### Read file

`GET /file/:id` Authorization required

**Response**

- `200 OK` on success

**Response headers**

- `Content-Type: {file's content-type}`

### Update file

`POST /file/:id` Authorization required

**Response**

- `200 OK` on success

```json
{
    "id": "10",
    "user_id": "6",
    "name": "file2.pdf",
    "mime": "application/pdf",
    "date_created": "2019-08-15 17:58:36",
    "date_modified": "2019-08-15 18:59:19"
}
```

### Delete file

`DELETE /file/:id` Authorization required

**Response**

- `200 OK` on success

```json
{
    "id": "10",
    "user_id": "6",
    "name": "file2.pdf",
    "mime": "application/pdf",
    "date_created": "2019-08-15 17:58:36",
    "date_modified": "2019-08-15 18:59:19"
}
```

## PHP packages used

* [PHP-JWT](https://github.com/firebase/php-jwt)

## Author

L. Campanari