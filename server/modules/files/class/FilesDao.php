<?php

class FilesDao {

    private $table;
    private $dao;
    
    function __construct() {
        $this->table = 'files';
        $this->dao = new Dao($this->table);
    }

    public function save($data) {
        return $this->dao->save($data, true);
    }

    public function find($id = null) {
        return $this->dao->get($id);
    }

    public function delete($id) {
        return $this->dao->delete($id);
    }

    public function findByUserId($userId) {
        $userId = System::sanitize($userId);
        $query = new stdClass;
        $query->select = "id, name, mime, date_created, date_modified";
        $query->where = "user_id = '$userId'";
        $query->order = "date_created DESC";
        return $this->dao->get($query);
    }

}

?>