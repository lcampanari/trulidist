<?php

class FilesApi {

    public static function upload() {
        try {
            $user = UserControl::authRequest();
        } catch (\Exception $e) {
            return Api::response(['error' => $e->getMessage()]);
        }

        $dao = new FilesDao;
        $body = Request::body();
        $file = $body['file'];
        $blob = file_get_contents($file['tmp_name']);
        $blobEnc = System::encryptFile($blob);
        $blobEnc = addslashes($blobEnc);
        $saved = $dao->save([
            'name' => $file['name'],
            'mime' => $file['type'],
            'data' => $blob,
            'user_id' => $user['id']
        ]);
        $saved = FilesControl::prepare($saved);
        return Api::response($saved);
    }

    public static function list() {
        try {
            $user = UserControl::authRequest();
        } catch (\Exception $e) {
            return Api::response(['error' => $e->getMessage()]);
        }
        $dao = new FilesDao;
        $list = $dao->findByUserId($user['id']);
        if (!empty($list) && !array_key_exists(0, $list)) {
            $list = [$list];
        }
        return Api::response($list);
    }

    public static function delete($id) {
        try {
            $user = UserControl::authRequest();
        } catch (\Exception $e) {
            return Api::response(['error' => $e->getMessage()]);
        }
        $id = System::sanitize($id);
        $dao = new FilesDao;
        $file = $dao->find($id);

        if (!$file) {
            return Api::response(null, null, 404);
        }

        $dao->delete($id);
        $deleted = FilesControl::prepare($file);
        return Api::response($deleted);
    }

    public static function edit($id) {
        try {
            $user = UserControl::authRequest();
        } catch (\Exception $e) {
            return Api::response(['error' => $e->getMessage()]);
        }
        $body = Request::body();
        $body['id'] = System::sanitize($id);
        $file = $body['file'];
        $blob = file_get_contents($file['tmp_name']);
        $dao = new FilesDao;
        $saved = $dao->save([
            'id'   => $body['id'],
            'name' => $file['name'],
            'mime' => $file['type'],
            'data' => $blob,
        ]);
        $saved = FilesControl::prepare($saved);
        return Api::response($saved);
    }

    public static function view($id) {
        try {
            $user = UserControl::authRequest();
        } catch (\Exception $e) {
            return Api::response(['error' => $e->getMessage()]);
        }
        $id = System::sanitize($id);
        $dao = new FilesDao;
        $file = $dao->find($id);
        $fileData = System::decryptFile($file['data']);
        return Api::response($fileData, $file['mime']);
    }

}
