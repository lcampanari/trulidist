<?php

class UserDao {

    private $table;
    private $dao;
    
    function __construct() {
        $this->table = 'user';
        $this->dao = new Dao($this->table);
    }

    public function save($data) {
        return $this->dao->save($data, true);
    }

    public function find($id = null) {
        $user = $this->dao->get($id);
        return $user;
    }

    public function findByUsername($username) {
        $query = new stdClass;
        $query->where = "username = '$username'";
        $user = $this->dao->get($query);
        return $user;
    }

    public function delete($id) {
        return $this->dao->delete($id);
    }

}

?>