<?php

class UserControl {

    public static function token($user) {
        $tokenPayload = ['userId' => $user['id']];
        return Api::generateToken($tokenPayload);
    }

    public static function prepare($user) {
        unset($user['password']);
        return $user;
    }

    public static function authRequest() {
        $payload = Api::validateToken();
        if (!$payload) {
            throw new Exception('Not authorized', 1);
        }
        if (!isset($payload['userId'])) {
            throw new Exception('Missing data', 1);
        }
        $dao = new UserDao;
        $user = $dao->find($payload['userId']);
        if (!$user) {
            throw new Exception('User does not exist', 1);
        }
        return self::prepare($user);
        
    }
}

?>