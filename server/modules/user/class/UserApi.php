<?php

class UserApi {

    public static function login() {
        $post = Request::body();

        if (empty($post['username'])) {
            return Api::response(['error' => 'Username is required.']);
        }
        if (empty($post['password'])) {
            return Api::response(['error' => 'Password is required.']);
        }

        $dao = new UserDao;
        $user = $dao->findByUsername($post['username']);

        if (!$user || $user['password'] != System::encrypt($post['password'])) {
            return Api::response(['error' => 'Wrong credentials.', 'user' => $user, 'p' => System::encrypt($post['password'])]);
        }
        $user['token'] = UserControl::token($user);
        $user = UserControl::prepare($user);
        return Api::response($user);
    }

    public static function register() {
        $post = Request::body();

        if (empty($post['username'])) {
            return Api::response(['error' => 'Username is required.']);
        }
        if (empty($post['password'])) {
            return Api::response(['error' => 'Password is required.']);
        }

        $dao = new UserDao;
        $user = $dao->findByUsername($post['username']);

        if ($user) {
            return Api::response(['error' => 'This user already exists.']);
        }

        $save = [
            'username' => $post['username'],
            'password' => System::encrypt($post['password'])
        ];

        $newUser = $dao->save($save);
        $newUser['token'] = UserControl::token($newUser);
        $newUser = UserControl::prepare($newUser);
        return Api::response($newUser);
    }

    public static function user($id = null) {
        if (!$id) {
            $payload = Api::validateToken();
            if (!$payload) {
                return Api::response(['error' => 'Not authorized']);
            }
            if (!isset($payload['userId'])) {
                return Api::response(['error' => 'Missing data']);
            }
            $id = $payload['userId'];
        }
        $dao = new UserDao;
        $user = $dao->find($id);
        if (!$user) {
            return Api::response(['error' => 'User not found']);
        }
        $user = UserControl::prepare($user);
        return Api::response($user);
    }

    // public function userByUsername() {
    //     $post = Request::body();
        
    // }

    

}
