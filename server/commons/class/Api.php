<?php

use \Firebase\JWT\JWT;

class Api {
    public const JSON = 'application/json';
    public const FORM_DATA = 'multipart/form-data';
    
    private const SECRET_KEY = "A Different Cogito Ergo Sum";

    public static $defaultHeaders = [
        'Access-Control-Allow-Origin'  => '*',
        'Content-Type' => 'application/json',
        'Access-Control-Allow-Headers' => 'Access-Control-Allow-Headers, Origin, X-Requested-With, Content-Type, Accept, Authorization, Cache-Control'
    ];

    public static function defaultHeaders() {
        foreach (self::$defaultHeaders as $header => $value) {
            header("$header: $value");
        }
    }

    public static function defaultResponse($data = null, $code = 200) {
        self::defaultHeaders();
        if ($code) {
            http_response_code($code);
        }
        if ($data !== null) {
            echo json_encode($data);
        }
    }

    
    public static function prepareResponse($body, $type = self::JSON, $code = 200) {
        $body = gettype($body) === 'string' 
            ? ['error' => $body] 
            : $body;
        return [
            'body' => $body,
            'status_code' => $code
        ];
    }

    public static function response($data, $type = self::JSON, $code = 200) {
        $data = self::encodeData($type, $data);
        // $headers = self::$defaultHeaders;
        $headers['Content-Type'] = $type;

        foreach ($headers as $header => $value) {
            header("$header: $value");
        }
        if ($code) {
            http_response_code($code);
        }
        if ($data) {
            echo $data;
        }

    }

    private static function encodeData($type, $data) {
        switch ($type) {
        case self::JSON:
            $data = json_encode($data);
            break;
        default:
            break;
        }
        return $data;
    }

    public static function generateToken($payload) {
        $default = [
            'iat' => time(),
            'iss' => System::getHost(),
            'exp' => time() + (365 * 24 * 60 * 60)
        ];
        $payload = array_merge($payload, $default);
        $token = JWT::encode($payload, self::SECRET_KEY);
        return $token;
    }

    public static function validateToken() {
        $token = self::getAuthToken();
        try {
            $payload = JWT::decode($token, self::SECRET_KEY, ['HS256']);
            $payload = (array) $payload;
            return $payload;
        } catch (\Throwable $th) {
            return false;
        }
    }

    public static function getAuthToken() {
        $header = Request::headers('Authorization');
        $token = is_string($header) ? str_replace('Bearer ','', $header) : "";
        return $token;
    }




}

?>