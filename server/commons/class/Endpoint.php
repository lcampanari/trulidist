<?php

class Endpoint {

    /**
     * @var $uri Original uri
     */
    public $uri;

    /**
     * @var $endpoint Matched endpoint
     */
    public $endpoint;

    /**
     * @var $id Matched id
     */
    public $id;

    /**
     * @var $response Endpoint response with http code
     */
    public $response;

    public $store = [];

    public function __construct($uri) {
        $this->uri = $uri;
        list($this->endpoint, $this->id) = $this->_prepareEndpoint($uri);
        return $this;
    }

    public function get($endpoint, $function) {
        $this->add('GET', $endpoint, $function);
    }

    public function post($endpoint, $function) {
        $this->add('POST', $endpoint, $function);
    }

    public function put($endpoint, $function) {
        $this->add('PUT', $endpoint, $function);
    }

    public function delete($endpoint, $function) {
        $this->add('DELETE', $endpoint, $function);
    }

    public function add($methods, $endpoint, $function) {
        if (gettype($methods) === 'string') {
            $methods = [$methods];
        }
        $this->store[$endpoint][] = ['methods' => $methods, 'function' => $function];
    }

    public function response() {
        $this->setResponseCode(400);
        foreach ($this->store as $endpoint => $arrayOfProps) {
            if ($endpoint == $this->endpoint) {
                foreach ($arrayOfProps as $i => $props) {
                    if (in_array(Request::method(), $props['methods'])) {
                        $function = $props['function'];
                        $args = $this->id ? [$this->id] : [];
                        $this->response = $function(...$args);
                        return $this->response;
                    }
                }
                $this->setResponseCode(405);
                return $this->response;
            }
        }
        return $this->response;
    }

    private function setResponseCode($code) {
        $this->response['status_code'] = $code;
        return $this;
    }

    private function _prepareEndpoint($uri) {
        $patternId = '/(\d+)/';
        preg_match($patternId, $uri, $matches);
        $id = !empty($matches[1])?$matches[1]:'';
        $endpoint = preg_replace($patternId, ':id', $uri);
        return [$endpoint, $id];
    }

}




?>