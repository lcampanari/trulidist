<?php

class Dao {

    private $string_sql;
    private $table;

    public function __construct($table) {
        $this->table = $table;
    }

    public function save($request, $return = false) {
        try {
            if (key_exists('id', $request)) {
                $id = $request['id'];
                unset($request['id']);
            }
            $table = $this->table;
            $result = $id ? $this->update($request, $id) : $this->insert($request);
            $id = $result && !$id ? $result : $id;
            return ($id && $return) ? $this->get($id) : $id;
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function insert(array $data) {
        try {
            $registry = Registry::getInstance();
            $conn = $registry->get();
            list($fields, $values) = $this->prepareDuplesForInsert($data);
            $table = $this->table;
            $sql = "INSERT INTO $table ($fields) VALUES ($values) ";
            $stmt = $conn->prepare($sql);
            foreach ($data as $key => $value) {
                $stmt->bindValue(":$key", $value);
            }
            $result = $stmt->execute();
            $lastId = $conn->lastInsertId();
            return $result ? $lastId : false;
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function update(array $data, $condition = "") {
        try {
            $registry = Registry::getInstance();
            $conn = $registry->get();
            $table = $this->table;
            if (is_string($table) && is_array($data)) {
                foreach ($data as $key => $value) {
                    $values[] = " $key = :$key ";
                }

                $sql = "";
                if ($values) {
                    $sql = "UPDATE $table  SET ";
                    $sql .= implode(",", $values);
                    if ($condition && is_numeric($condition)) {
                        $sql .= " WHERE id= $condition ";
                    } else if ($condition && is_string($condition)) {
                        $sql .= " WHERE $condition ";
                    }
                }
                $this->setStringSql($sql, $data);
                $stmt = $conn->prepare($sql);
                foreach ($data as $key => $value) {
                    $stmt->bindValue(":$key", $value);
                }
                $result = $stmt->execute();
                return $result ? $result : false;
            } else {
                return false;
            }
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function get($query, $flag_single = false, $fetch = PDO::FETCH_ASSOC) {
        try {
            $registry = Registry::getInstance();
            $conn = $registry->get();

            $table = $this->table;
            if (is_numeric($query) && $table) {
                $id = $query;
                $query = "SELECT * FROM $table WHERE id = :id";
            } elseif ($query instanceof stdClass) {
                $query = $this->queryFromObject($query);
            } elseif (!$query) {
                $query = "SELECT * FROM $table";
            }
            $stmt = $conn->prepare($query);
            if ($id) {
              $stmt->bindParam(':id', $id);
            }
            $result = $stmt->execute();
            if ($fetch) {
                $flag_single = $id ? true : $flag_single;
                $rows = $flag_single ? $stmt->fetch($fetch) : $stmt->fetchAll($fetch);
            }
            if ($rows && array_key_exists(0, $rows) && !array_key_exists(1, $rows)) {
                return $rows[0];
            } elseif ($rows) {
                return $rows;
            } else {
                return [];
            }
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function delete($condition = "") {
        try {
            $registry = Registry::getInstance();
            $conn = $registry->get();
            $table = $this->table;
            if ($condition) {
                $sql = " DELETE FROM `$table` ";
                if (is_numeric($condition)) {
                    $sql .= " WHERE id = $condition ";
                } else if (is_string($condition)) {
                    $sql .= " WHERE $condition ";
                }
                return $this->executeQuery($sql);
            } else {
                return false;
            }
        } catch (PDOException $e) {
            return false;
            // throw new Exception($e->getMessage());
        }
    }

    public function executeQuery($sql, $flag_single = false, $fetch = null, $bind_values = []) {
        try {
            $registry = Registry::getInstance();
            $conn = $registry->get();
            $stmt = $conn->prepare($sql);

            if(!empty($bind_values)) {
                if (key_exists(0, $bind_values)) {
                    foreach ($bind_values as $key => $value) {
                        $stmt->bindValue($key + 1, $value);
                    }
                } else {
                    foreach ($bind_values as $key => $value) {
                        $key = System::filterNameField($key);
                        $stmt->bindValue(":$key", $value);
                    }
                }
            }
            $result = $stmt->execute();

            $rows = null;
            if ($fetch) {
                $rows = ($flag_single) ? $stmt->fetch($fetch) : $stmt->fetchAll($fetch);
                $result = $rows ? $rows : array();
                $result = $flag_single && $result ? $result : false;
            }
            return $result;
        } catch (PDOException $e) {
            throw new Exception($e->getMessage());
        }
    }

    public function setStringSql($string_sql, $data = []) {
        foreach ($data as $key => $value) {
            $string_sql = preg_replace("/:$key/", "'$value'", $string_sql);
        }
        $this->string_sql = $string_sql;
    }

    private function prepareDuplesForInsert($data) {
        $fields = $values = "";
        $end_length = count($data) - 1;
        $i = 0;
        foreach ($data as $key => $value) {
            if ($i < $end_length) {
                $fields .= "$key,";
                $values .= ":$key,";
            } else {
                $fields .= "$key";
                $values .= ":$key";
            }
            $i++;
        }
        return [$fields, $values];
    }

    public function queryFromObject(stdClass $query) {
        $select = $query->select ? "SELECT {$query->select} " : "SELECT * ";
        
        if ($query->from) {
            $from = "FROM `{$query->from}` ";
        } else if ($this->table) {
            $from = "FROM `{$this->table}` ";
        } else {
            $from = '';
        }

        $where = $query->where ? "WHERE {$query->where} " : "";
        $order = $query->order ? "ORDER BY {$query->order} " : "";
        return $select.$from.$where.$order;
    }

}

?>
