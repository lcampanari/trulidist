<?php 

class Modules {

    public static $modules;

    public static function directories() {
        $dir = DIR_MODULES;
        $children = scandir($dir);
        $dirs = [];
        
        foreach ($children as $child) {
            if (!in_array($child, [".",".."]) && is_dir($dir.DS.$child)) {
                $dirs[] = $child.DS;
            }
        }
        return $dirs;
    }

}

?>