<?php

class System {

    public static function getProtocol() {
        if (isset($_SERVER['HTTPS'])) {
            return (($_SERVER['HTTPS'] == "on") ? "https://" : "http://");
        } else {
            return "http://";
        }
    }

    public static function getHost() {
        return !empty($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : "";
    }

    public static function getUrlRoot($folder_sys = FOLDER_SYS) {
        return System::getProtocol() . System::getHost() . '/' . $folder_sys;
    }

    public static function getUrlCurrent() {
        return System::getProtocol() . System::getHost() . $_SERVER['REQUEST_URI'];
    }

    public static function getUrlFileCurrent() {
        return System::getProtocol() . System::getHost() . $_SERVER['PHP_SELF'];
    }

    public static function getUrlFileFullParam() {
        return System::getProtocol() . System::getHost() . $_SERVER["REQUEST_URI"];
    }

    public static function passwordGenerator($numberChars = 8) {
        $chars = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'x', 'y', 'z', 'w', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0');
        $new_password = "";
        for ($i = 0; $i < $numberChars; $i++) {
            $key = rand(0, 35);
            $new_password .= $chars[$key];
        }
        return $new_password;
    }

    public static function encrypt($string, $secret_key = CRYPT_KEY, $secret_iv = CRYPT_TEXT) {
        return System::encrypt_decrypt('encrypt', $string, $secret_key, $secret_iv);
    }

    public static function decrypt($string, $secret_key = CRYPT_KEY, $secret_iv = CRYPT_TEXT) {
        return System::encrypt_decrypt('decrypt', $string, $secret_key, $secret_iv);
    }

    public static function encryptFile($file) {
        return System::encrypt($file, CRYPT_KEY_FILES, CRYPT_TEXT_FILES);
    }

    public static function decryptFile($file) {
        return System::decrypt($file, CRYPT_KEY_FILES, CRYPT_TEXT_FILES);
    }

    /**
     * simple method to encrypt or decrypt a plain text string
     * initialization vector(IV) has to be the same when encrypting and decrypting
     * 
     * @param string $action: can be 'encrypt' or 'decrypt'
     * @param string $string: string to encrypt or decrypt
     *
     * @return string
     */
    public static function encrypt_decrypt($action, $string, $secret_key = CRYPT_KEY, $secret_iv = CRYPT_TEXT) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        if ($action == 'encrypt') {
            $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }
        if (!$output) {
            return $string;
        }
        return $output;
    }

    public static function isInteger($input) {
        return(ctype_digit(strval($input)));
    }

    public static function cleanBreakLine($input){
        $search=  array("\n", "\r");
        $replace= array("", "");
        return str_replace($search, $replace, $input);
    }

    public static function  filterNameField($str, $quote=false) {
        $result = preg_replace('/[^a-zA-Z0-9_.]/', '', $str);
        if ($quote) {
            if((strpos($result, '.') !== false)){
                return str_replace(array('`','.'),array('','.`'),$result).'`';
            }else{
                return '`'.$result.'`';
            }
        }
        return $result;
    }

    public static function sanitize($input,$disable_breaks=false,$disable_quotes=false,$disable_tags=false,$disable_filter=false) {
        if(is_array($input)){
           foreach ($input as $key=>$value){
                $input[$key]= System::sanitize($value,$disable_breaks,$disable_quotes,$disable_tags,$disable_filter);
           } 
           return $input;
        }
        if(!empty($input) && is_string($input)) {
            $search  = array('\\', "\0","\x1a",chr(0));
            $replace = array(''  , ''  ,'\\Z' , '');
            if(!$disable_quotes){
                $search  = array_merge($search,array(" '" , "' ",' "','" ',"'",'"'));
                $replace = array_merge($replace,array(" ‘", "’ ",' “','” ','' ,''));
            }
            $input = trim(str_replace( $search,$replace , $input));
            $input = !$disable_tags?strip_tags($input):$input;
            $input = !$disable_breaks?System::cleanBreakLine($input):$input;
            $input = !$disable_filter?filter_var($input,FILTER_SANITIZE_STRING):$input;
        }
        return $input;
    }

    public static function  filterInteger($num) {
        $num=System::sanitize($num);
        $num = preg_replace('/[^0-9]/', '',  $num);
        return System::isInteger($num)?$num:0;
    }

    /**
     * Filter array based on keys
     * @param array $array to be filtered
     * @param array $regexFilters array of regular expressions
     */
    public static function array_filter_keys($array, $regexFilters = []) {
        $filtered = array_filter($array, function($key) use($regexFilters) {
            $isMatch = false;
            foreach ($regexFilters as $pattern) {
                $isMatch = preg_match($pattern, $key);
                if ($isMatch) {
                    break;
                }
            }
            return $isMatch;
        }, ARRAY_FILTER_USE_KEY);
        return $filtered;
    }

    public static function getId(){
        if(!empty($_REQUEST['id'])){
            return System::filterInteger($_REQUEST['id']);
        }
        $json = Request::body(true);
        if(!empty($json->id)){
            return System::filterInteger($json->id);
        }
        return 0;
    }

    public static function preg_grep_keys($pattern, $array) {
        $matches = preg_grep($pattern, array_keys($array));
        $arr = [];
        foreach($matches as $match) {
            $arr[$match] = $array[$match];
        }
        return $arr;
    }

}

?>
