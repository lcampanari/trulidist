<?php

// Singleton to connect db.
class Connection {

  private static $instance = null;
  private $conn;

  private function __construct() {  
    $options = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8mb4'
    ); 
    $pdo = new PDO(DB_TYPE.":host=".DB_HOST.";port=".DB_PORT.";dbname=".DB_NAME, DB_USER, DB_PASS, $options);   
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $this->conn = $pdo;
  }
  
  public static function getInstance() {
    if(!self::$instance) {
      self::$instance = new Connection();
    }
    return self::$instance;
  }
  
  public function getConnection() {
    return $this->conn;
  }
  
  public function close() {
    $this->conn = null;
  }
}