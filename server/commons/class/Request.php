<?php

class Request {

    public static function post($url, $data, $header = []) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if (!empty($header)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        }
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    public static function get($url, $header = []) {
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if (!empty($header)) {
            curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        }
        $response = curl_exec($curl);
        curl_close($curl);
        return $response;
    }

    public static function body(bool $object = false, bool $sanitize = true, bool $isJson = true) {
        $body = file_get_contents('php://input');

        if ($isJson) {
            $body = json_decode($body, true);
        }

        $body = $body ? $body : [];
        if (!empty($_FILES)) {
            $body = array_merge($body, $_FILES);
        }
        
        if ($sanitize) {
            $body = System::sanitize($body);
        }
        if ($isJson && $object) {
            $body = json_decode(json_encode($body));
        }
        return $body;
    }

    public static function headers($name) {
        $headers = getallheaders();
        return  !empty($headers[$name]) ? $headers[$name] : $headers;
    }

    public static function method() {
        return $_SERVER['REQUEST_METHOD'];
    }

}

?>
