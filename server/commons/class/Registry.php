<?php

class Registry {

   private static $instance;

   private $storage;

   protected function __construct() {
      $this->storage = new ArrayObject;
   }

   /**
    * @param string $key
    * @return mixed Stored value
    * @throws RuntimeException
    */
   public function get($key = null) {
    if (empty($key)) {
        $key = 'mysql';
     }
     if ($this->storage->offsetExists($key)) {
        return $this->storage->offsetGet($key);
     } else {
        throw new RuntimeException('Invalid key.');
     }
   }

   /**
    * @return Registry
    */
   public static function getInstance() {
      if (!self::$instance)
         self::$instance = new Registry();

      return self::$instance;
   }

   /**
    * @param string $key
    * @param mixed $value
    * @throws LogicException
    */
   public function set($key, $value) {
      if (!$this->storage->offsetExists($key)) {
         $this->storage->offsetSet($key, $value);
      } else {
         throw new LogicException('This key already exists.');
      }
   }

   /**
    * @param string $key
    * @throws RuntimeException
    */
   public function unregister($key) {
      if ($this->storage->offsetExists($key)) {
         $this->storage->offsetUnset($key);
      } else {
         throw new RuntimeException('Invalid key.');
      }
   }

}
