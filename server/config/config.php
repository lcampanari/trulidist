<?php

try {
    define('IS_LOCALHOST', 
        isset($_SERVER['REMOTE_ADDR']) && in_array($_SERVER['REMOTE_ADDR'], ['127.0.0.1', '::1'])
    );
    define('DS', DIRECTORY_SEPARATOR);

    require_once 'db.config.php';

    if (IS_LOCALHOST) {
        define('FOLDER_SYS', 'trulidist/server/');
    } else {
        define('FOLDER_SYS', 'server/');
    }

    if(!empty($_SERVER['DOCUMENT_ROOT'])) {
        define('DIR_SYS', $_SERVER['DOCUMENT_ROOT'].DS.FOLDER_SYS);
    } else {
        define('DIR_SYS', __DIR__.'/../'.FOLDER_SYS);
    }

    define('DIR_MODULES', DIR_SYS.'modules/');
    define('DIR_COMMONS', DIR_SYS.'commons/');
    define('DIR_UPLOAD', DIR_SYS.FOLDER_UPLOAD);
    define('FOLDER_CLASS', 'class/');
    define('FOLDER_UPLOAD', 'uploads/');
    define('TITLE_WEB', 'TruliDist');

    require_once DIR_COMMONS.'class/Modules.php';
    require_once DIR_COMMONS.'class/Registry.php';
    require_once DIR_COMMONS.'class/Connection.php';
    $registry = Registry::getInstance();
    $registry->set('modules', Modules::directories());
    $registry->set(DB_TYPE, Connection::getInstance()->getConnection());
    
    require_once DIR_SYS . 'config/autoload.php';
    require_once DIR_SYS . 'vendor/autoload.php';

    define('CRYPT_KEY','1875391847322019');
    define('CRYPT_TEXT','Gottfried Wilhelm Leibniz');
    define('CRYPT_KEY_FILES','1918328753472019');
    define('CRYPT_TEXT_FILES','Something Else Other Than Leibniz. Maybe Newton?');
} catch (Exception $e) {
    echo $e->getMessage();
}
?>