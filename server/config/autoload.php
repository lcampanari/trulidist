<?

class Autoloader {

    public static function classLoader($class) {
        $classname = $class;
        $directories[] = DIR_COMMONS.FOLDER_CLASS;
        $modules = Registry::getInstance()->get('modules');
        foreach ($modules as $module) {
            $directories[] = DIR_MODULES.$module.FOLDER_CLASS;
        }
        self::searchClass($directories, $classname);
    }

    private static function searchClass($directories, $classname) {
        foreach ($directories as $dir_current) {
            if (file_exists($dir_current) && is_dir($dir_current)) {
                $dir_handle = opendir($dir_current);
                while ($file = readdir($dir_handle)) {
                    if ($file != "." && $file != "..") {
                        if (substr($file, 0, strpos($file, ".")) == $classname) {
                            return require_once($dir_current.$classname.".php");
                        }
                    }
                }
                closedir($dir_handle);
            }
        }
    }
}

spl_autoload_register('Autoloader::classLoader');
?>