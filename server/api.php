<?php
require_once 'config/config.php';
$allowOrigin = IS_LOCALHOST ? '*' : 'http://bluepineapple.xyz';

header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept, Authorization, Cache-Control');
header('Access-Control-Allow-Methods: POST, GET, PUT, DELETE');
header("Access-Control-Allow-Origin: $allowOrigin");



try {
    $uri = !empty($_REQUEST['uri']) ? $_REQUEST['uri'] : "";
    $endpoint = new Endpoint($uri);
    $endpoint->post('/login', 'UserApi::login');
    $endpoint->post('/register', 'UserApi::register');
    $endpoint->get('/user', 'UserApi::user');
    $endpoint->get('/files', 'FilesApi::list');
    $endpoint->post('/file', 'FilesApi::upload');
    $endpoint->get('/file/:id', 'FilesApi::view');
    $endpoint->post('/file/:id', 'FilesApi::edit');
    $endpoint->delete('/file/:id', 'FilesApi::delete');
    $endpoint->response();
} catch (Exception $e) {
    echo $e->getMessage();
}
?>