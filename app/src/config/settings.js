const UPLOAD_ENDPOINT = '/server/uploads'

export const URL_BASE = `${process.env.REACT_APP_URL_BASE}`
export const URL_API = `${process.env.REACT_APP_URL_API}`
export const URL_UPLOAD = `${process.env.REACT_APP_URL_BASE}${UPLOAD_ENDPOINT}`