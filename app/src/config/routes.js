import { Dashboard, Home, Signup } from '../pages'

const routes = {
  Home: {
    path: '/',
    component: Home,
    exact: true
  },
  Dashboard: {
    path: '/dashboard',
    component: Dashboard
  },
  Signup: {
    path: '/signup',
    component: Signup
  }
}

export default routes
