import React, { useContext, useState } from 'react'
import { Alert, Button, Col, Container, Form, Row } from 'react-bootstrap'
import { Link, Redirect } from 'react-router-dom'
import Head from '../../components/Head'
import routes from '../../config/routes'
import AppContext from '../../contexts/AppContext'
import './Login.css'

const Login = () => {
  const [loading, setLoading] = useState(false)
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState('')
  const {
    login,
    state: { isSignedIn }
  } = useContext(AppContext)

  const submit = () => {
    setLoading(true)
    setError('')
    login(username, password).then(res => {
      if (res.error) {
        setError(res.error)
        setLoading(false)
      }
    })
  }

  function handleChange (e) {
    let { id: field, value } = e.currentTarget
    switch (field) {
      case 'username':
        setUsername(value)
        break
      case 'password':
        setPassword(value)
        break
      default:
        break
    }
  }

  if (isSignedIn) {
    return (
      <Redirect
        to={{
          pathname: routes.Dashboard.path
        }}
      />
    )
  }

  return (
    <>
      <Head />
      <Container className='login-page'>
        <Row className='justify-content-center'>
          <Col
            xs={12}
            md={5}
            className='border rounded text-left p-3 px-4'
            // style={{ maxWidth: '400px' }}
          >
            <h3 className='font-weight-light mb-3'>Sign In</h3>
            {error && <Alert variant='danger'>{error}</Alert>}
            <Form>
              <Form.Group controlId='username'>
                <Form.Label>Username</Form.Label>
                <Form.Control
                  type='email'
                  placeholder='Enter username'
                  onChange={handleChange}
                />
              </Form.Group>

              <Form.Group controlId='password'>
                <Form.Label>Password</Form.Label>
                <Form.Control
                  type='password'
                  placeholder='Enter password'
                  onChange={handleChange}
                />
              </Form.Group>
              <Link className='float-right' to={routes.Signup.path}>
                <small>Create an account</small>
              </Link>
              <Button
                variant='primary'
                type='button'
                onClick={() => submit()}
                disabled={loading}
                className='mt-2'
              >
                Enter
              </Button>
            </Form>
          </Col>
        </Row>
      </Container>
    </>
  )
}

export default Login
