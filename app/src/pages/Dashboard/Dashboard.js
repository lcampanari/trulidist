import React from 'react'
import { Container } from 'react-bootstrap'
import Head from '../../components/Head'
import Header from '../../components/Header'
import { FilesProvider } from '../../contexts/FilesContext'
import './Dashboard.css'
import Files from './Files'
import NewFile from './NewFile'

const Dashboard = () => {
  return (
    <>
      <FilesProvider>
        <Head />
        <Header />
        <Container className='dashboard-page'>
          <NewFile />
          <Files />
        </Container>
      </FilesProvider>
    </>
  )
}

export default Dashboard
