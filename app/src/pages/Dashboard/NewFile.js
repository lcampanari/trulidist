import { mdiCheck } from '@mdi/js'
import Icon from '@mdi/react'
import React, { useContext, useState } from 'react'
import { Button, Card, Col, Form, Row, Spinner } from 'react-bootstrap'
import { URL_API } from '../../config/settings'
import FilesContext from '../../contexts/FilesContext'
import UserControl from '../../controllers/UserControl'

const NewFile = ({ updateList }) => {
  const [file, setFile] = useState(null)
  const [uploading, setUploading] = useState(false)
  const { append } = useContext(FilesContext)

  function handleSubmit (e) {
    e.preventDefault()
    setUploading(true)
    const data = new FormData(e.target)
    return fetch(`${URL_API}/file`, {
      method: 'post',
      headers: {
        Authorization: `Bearer ${UserControl.getToken()}`
      },
      body: data
    })
      .then(res => res.json())
      .then(res => {
        append(res)
        setFile(null)
        setUploading(false)
        document.getElementById('file').value = ''
      })
      .catch(handleSubmitError)
  }

  function handleSubmitError (err) {
    setFile(null)
    setUploading(false)
    document.getElementById('file').value = ''
    alert('Unexpected error. Please try another file.')
  }

  function handleChange (e) {
    const form = document.getElementById('newFile')
    const data = new FormData(form)
    setFile(data.get('file').name)
  }

  let fileInput

  return (
    <>
      <Row className='mt-4'>
        <Col>
          <h1 className='font-weight-light mb-4'>New file</h1>
          <Form id='newFile' onSubmit={handleSubmit}>
            <Card
              className={`new-file ${uploading ? 'blocked' : ''}`}
              onClick={() => (!uploading ? fileInput.click() : null)}
            >
              <Card.Body className='d-flex justify-content-center align-items-center'>
                {file ? (
                  <>
                    {file}
                    <Icon
                      path={mdiCheck}
                      size={1}
                      color='green'
                      className='ml-2'
                    />
                  </>
                ) : (
                  'Click here to upload a new file'
                )}
              </Card.Body>
              <Form.Group controlId='file' className='upload-form'>
                <Form.Control
                  type='file'
                  name='file'
                  ref={ref => (fileInput = ref)}
                  onChange={handleChange}
                />
              </Form.Group>
            </Card>
            <Button
              variant='success'
              className='mt-3 float-right'
              type='submit'
              disabled={uploading || !file}
              style={{ width: '100px' }}
            >
              {uploading ? (
                <Spinner animation='border' size='sm' as='span' />
              ) : (
                'Upload'
              )}
            </Button>
          </Form>
        </Col>
      </Row>
    </>
  )
}

export default NewFile
