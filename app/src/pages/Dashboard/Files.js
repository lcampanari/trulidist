import React, { useContext, useEffect, useState } from 'react'
import { Col, ListGroup, Row, Spinner } from 'react-bootstrap'
import FilesContext from '../../contexts/FilesContext'
import FilesList from './FilesList'

const Files = () => {
  const { load, state, replace } = useContext(FilesContext)
  const [replaceId, setReplaceId] = useState(null)
  const [downloading, setDownloading] = useState(false)
  const replaceForm = React.createRef()

  useEffect(() => {
    load()
  }, [])

  return (
    <>
      <Row className='py-5 pt-4'>
        <Col>
          <h1 className='font-weight-light mb-4'>Files</h1>
          <ListGroup variant=''>
            {state.loading
              ? LoadingList()
              : FilesList({
                list: state.list,
                replaceForm,
                replaceId,
                setReplaceId,
                downloading,
                setDownloading
              })}
          </ListGroup>
        </Col>
      </Row>
    </>
  )
}

export default Files

function LoadingList () {
  return (
    <ListGroup.Item className='text-center'>
      <Spinner animation='border' />
    </ListGroup.Item>
  )
}
