import {
  mdiDeleteOutline,
  mdiDownloadOutline,
  mdiFileReplaceOutline
} from '@mdi/js'
import Icon from '@mdi/react'
import axios from 'axios'
import { saveAs } from 'file-saver'
import React, { useContext } from 'react'
import { Button, Form, ListGroup } from 'react-bootstrap'
import { URL_API } from '../../config/settings'
import AppContext from '../../contexts/AppContext'
import FilesContext from '../../contexts/FilesContext'
import UserControl from '../../controllers/UserControl'

export default function FilesList ({
  list,
  replaceId,
  setReplaceId,
  downloading,
  setDownloading
}) {
  const { state: appState } = useContext(AppContext)
  const { remove, replace } = useContext(FilesContext)
  let replaceForm

  function submitForm (e) {
    const form = document.getElementById('replace')
    const data = new FormData(form)
    const id = replaceId
    return replace(id, data)
  }
  function removeItem (e) {
    return remove(e.currentTarget.id)
  }
  function replaceItem (e) {
    replaceForm.click()
    setReplaceId(e.currentTarget.id)
  }
  function downloadItem (file, e) {
    setDownloading(true)
    return fetchBlob(file.id).then(blob => {
      if (blob) {
        saveAs(blob, file.name)
        setDownloading(false)
      }
    })
  }

  async function fetchBlob (id) {
    return await axios(`${URL_API}/file/${id}`, {
      method: 'get',
      headers: {
        Authorization: `Bearer ${UserControl.getToken()}`
      },
      responseType: 'blob'
    }).then(res => {
      if (res) {
        const file = new Blob([res.data], { type: res.headers['content-type'] })
        return file
      }
    })
  }

  if (list.length === 0) {
    return <ListGroup.Item className='text-center'>No files</ListGroup.Item>
  }

  return (
    <>
      <Form id='replace'>
        <Form.Group controlId='fileReplace' className='upload-form'>
          <Form.Control
            type='file'
            name='file'
            ref={ref => (replaceForm = ref)}
            onChange={submitForm}
          />
        </Form.Group>
      </Form>

      {list.map((file, i) => (
        <ListGroup.Item key={i} className='files-item'>
          <div className='file-info'>
            <div className='file-name'>{file.name}</div>
            <div className='d-flex flex-wrap text-muted'>
              <small className='pr-3 pt-1'>
                <span className='font-weight-bold'>Created </span>
                {file.date_created}
              </small>
              {file.date_modified && (
                <small className='pt-1'>
                  <span className='font-weight-bold'>Modified </span>
                  {file.date_modified}
                </small>
              )}
            </div>
          </div>
          <div className='file-actions'>
            <div className='file-buttons'>
              <Button
                size='sm'
                variant='light'
                className='mr-1'
                onClick={e => downloadItem(file)}
                dataname={file.name}
                disabled={downloading}
              >
                <Icon path={mdiDownloadOutline} size={1} color='#5f5f5f' />
              </Button>
              {!!+appState.user.allow_edit && (
                <Button
                  size='sm'
                  variant='light'
                  className='mr-1'
                  onClick={replaceItem}
                  id={file.id}
                >
                  <Icon path={mdiFileReplaceOutline} size={1} color='#5f5f5f' />
                </Button>
              )}
              <Button
                size='sm'
                variant='light'
                onClick={removeItem}
                id={file.id}
              >
                <Icon path={mdiDeleteOutline} size={1} color='#5f5f5f' />
              </Button>
            </div>
          </div>
        </ListGroup.Item>
      ))}
    </>
  )
}
