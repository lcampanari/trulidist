import React, { Component } from 'react'
import { Preloader } from 'react-materialize'
import Head from '../components/Head'
import { LayoutControl } from '../controllers'

export default class Terms extends Component {
  constructor (props) {
    super(props)
    this.state = { page: {} }
  }

  prepare = () => {
    LayoutControl.getTerms().then(page => this.setState({ page }))
  }

  componentDidMount () {
    this.prepare()
    window.scrollTo(0, 0)
  }

  createMarkup (html) {
    return { __html: html }
  }

  render () {
    return (
      <React.Fragment>
        {this.state.page ? (
          <div>
            <Head
              title={this.state.page.title}
              description={this.state.page.short_description}
            />
            <div className='pageContent'>
              <div className='container'>
                <div className='row'>
                  <div className='col s12'>
                    <h1 className='text-main-color line-bottom'>
                      {this.state.page.title}
                    </h1>
                    <div
                      dangerouslySetInnerHTML={this.createMarkup(
                        this.state.page.text
                      )}
                    />
                  </div>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <Preloader className='center' />
        )}
      </React.Fragment>
    )
  }
}
