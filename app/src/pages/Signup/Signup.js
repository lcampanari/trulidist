import React, { useContext, useState } from 'react'
import { Alert, Button, Col, Container, Form, Row } from 'react-bootstrap'
import { Link, Redirect } from 'react-router-dom'
import Head from '../../components/Head'
import routes from '../../config/routes'
import AppContext from '../../contexts/AppContext'
import '../Login/Login.css'

const Signup = () => {
  const [loading, setLoading] = useState(false)
  const [success, setSuccess] = useState(false)
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [error, setError] = useState('')
  const {
    signup,
    state: { isSignedIn }
  } = useContext(AppContext)

  const submit = () => {
    setLoading(true)
    setError('')
    signup(username, password).then(res => {
      console.log(res)

      if (res.error) {
        setError(res.error)
        setLoading(false)
        setSuccess(false)
      } else {
        setSuccess(true)
      }
    })
  }

  function handleChange (e) {
    let { id: field, value } = e.currentTarget
    switch (field) {
      case 'username':
        setUsername(value)
        break
      case 'password':
        setPassword(value)
        break
      default:
        break
    }
  }

  return (
    <>
      <Head />

      {success ? (
        <Redirect
          to={{
            pathname: routes.Dashboard.path
          }}
        />
      ) : (
        <Container className='login-page'>
          <Row className='justify-content-center'>
            <Col xs={12} md={5} className='border rounded text-left p-3 px-4'>
              <h3 className='font-weight-light mb-3'>Sign up</h3>
              {error && <Alert variant='danger'>{error}</Alert>}
              <Form>
                <Form.Group controlId='username'>
                  <Form.Label>Username</Form.Label>
                  <Form.Control
                    type='email'
                    placeholder='Choose username'
                    onChange={handleChange}
                  />
                </Form.Group>

                <Form.Group controlId='password'>
                  <Form.Label>Password</Form.Label>
                  <Form.Control
                    type='password'
                    placeholder='Choose password'
                    onChange={handleChange}
                  />
                </Form.Group>
                <Link className='float-right' to={routes.Home.path}>
                  <small>Have an account? Sign in.</small>
                </Link>
                <Button
                  variant='success'
                  type='button'
                  onClick={() => submit()}
                  disabled={loading}
                  className='mt-2'
                >
                  Sign up
                </Button>
              </Form>
            </Col>
          </Row>
        </Container>
      )}
    </>
  )
}

export default Signup
