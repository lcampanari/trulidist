import React, { Component } from 'react'
import logo from './logo.svg'
import { AppProvider } from './contexts/AppContext'
import './App.css'

class App extends Component {
  render () {
    return <AppProvider>{this.props.children}</AppProvider>
  }
}
export default App
