import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import App from './App'
import InvalidRoute from './components/Route/InvalidRoute'
import PrivateRoute from './components/Route/PrivateRoute'
import routes from './config/routes'
import * as serviceWorker from './serviceWorker'

process.title = 'trulidist'

ReactDOM.render(
  <Router>
    <App>
      <Switch>
        <Route {...routes.Home} />
        <Route {...routes.Signup} />
        <PrivateRoute {...routes.Dashboard} />
        <InvalidRoute />
      </Switch>
    </App>
  </Router>,
  document.getElementById('root')
)

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister()
