import React, { Component } from 'react'
import { v1 } from '../api'
import { URL_API } from '../config/settings'
import UserControl from '../controllers/UserControl'

const FilesContext = React.createContext()

class FilesProvider extends Component {
  state = {
    list: [],
    loading: false,
    error: '',
    loadingReplace: false,
    loadingDownload: false
  }

  append = item => {
    let { list } = this.state
    list.unshift(item)
    this.setState({ list })
  }

  deleteFromList = id => {
    let { list } = this.state
    list.splice(list.findIndex(item => item.id === id), 1)
    this.setState({ list })
  }

  replaceFromList = (id, item) => {
    let { list } = this.state
    list.find((o, i) => {
      if (o.id === id) {
        list[i] = item
        return true
      }
    })

    this.setState({ list, loadingReplace: false })
  }

  remove = id => {
    return v1
      .delete(`/file/${id}`, UserControl.getToken())
      .then(res => (!res.error ? this.deleteFromList(id) : null))
  }

  replace = (id, data) => {
    this.setState({ loadingReplace: true })
    return fetch(`${URL_API}/file/${id}`, {
      method: 'post',
      headers: {
        Authorization: `Bearer ${UserControl.getToken()}`
      },
      body: data
    })
      .then(res => res.json())
      .then(res => (!res.error ? this.replaceFromList(id, res) : null))
  }

  load = () => {
    this.setState({ loading: true })
    return v1.getJson('/files', UserControl.getToken()).then(res => {
      if (res.error) {
        this.setState({ error: res.error, loading: false })
      } else {
        this.setState({ list: res, loading: false })
      }
    })
  }

  render () {
    return (
      <FilesContext.Provider
        value={{
          state: this.state,
          load: this.load,
          append: this.append,
          remove: this.remove,
          replace: this.replace
        }}
      >
        {this.props.children}
      </FilesContext.Provider>
    )
  }
}

export { FilesProvider }

export default FilesContext
