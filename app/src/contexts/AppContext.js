import React, { Component } from 'react';
import { getUser } from '../api';
import UserControl from '../controllers/UserControl';

const AppContext = React.createContext()

class AppProvider extends Component {
  state = {
    isSignedIn: UserControl.isSignedIn(),
    user: null
  }

  updateUser = field => {
    const user = { ...this.state.user, ...field }
    this.setState({ user })
  }

  setIsSignedIn = isSignedIn => {
    if (!isSignedIn) {
      UserControl.signOut()
    }
    this.setState({ isSignedIn })
  }

  setUser = user => {
    this.setState({ user })
  }

  getUser = () => {
    if (this.state.isSignedIn) {
      getUser(UserControl.getToken()).then(user => {
        if (!user.error) {
          this.setState({ user })
        }
      })
    }
  }

  login = (username, password) => {
    return UserControl.signIn({ username, password }).then(res => {
      if (!res.error && res.user) {
        this.setState({
          isSignedIn: true,
          user: res.user
        })
      }
      return res
    })
  }

  signup = (username, password) => {
    return UserControl.register({ username, password }).then(res => {
      if (!res.error && res.user) {
        this.setState({
            isSignedIn: true,
          user: res.user
        })
      }
      return res
    })
  }

  logout = () => {
    UserControl.signOut()
    this.setState({ isSignedIn: false }, () => (window.location.href = '/'))
  }

  componentDidMount () {
    this.getUser()
  }

  render () {
    return (
      <AppContext.Provider
        value={{
          state: this.state,
          setState: this.setState,
          setIsSignedIn: this.setIsSignedIn,
          setUser: this.setUser,
          updateUser: this.updateUser,
          login: this.login,
          signup: this.signup,
          logout: this.logout
        }}
      >
        {this.props.children}
      </AppContext.Provider>
    )
  }
}

export { AppProvider };

export default AppContext
