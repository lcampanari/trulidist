import React, { useContext } from 'react'
import { Container, Nav, Navbar } from 'react-bootstrap'
import AppContext from '../contexts/AppContext'
import Head from './Head'

const Header = () => {
  const { logout } = useContext(AppContext)

  return (
    <>
      <Head />
      <Navbar bg='light' variant='light'>
        <Container>
          <Navbar.Brand href='/'>TruliDist</Navbar.Brand>
          <Nav className='mr-auto'>
            <Nav.Link href='/dashboard'>Home</Nav.Link>
          </Nav>
          <Nav>
            <Nav.Link href='#' onClick={() => logout()}>
              Logout
            </Nav.Link>
          </Nav>
        </Container>
      </Navbar>
    </>
  )
}

export default Header
