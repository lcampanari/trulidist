import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import UserControl from "../../controllers/UserControl";

export default function PrivateRoute ({
  component: Component,
  redirectTo,
  ...rest
}) {
  return (
    <Route
      {...rest}
      render={props =>
        UserControl.isSignedIn() ? (
          <Component {...props} />
        ) : (
          <Redirect
            to={{
              pathname: '/login',
              state: { from: props.location },
              ...redirectTo
            }}
          />
        )
      }
    />
  )
}
