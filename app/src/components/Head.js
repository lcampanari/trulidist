import React from 'react'
import Helmet from 'react-helmet'

const Head = props => {
  const title = props.title || ''
  const description = props.description ? props.description : ''
  return (
    <Helmet>
      <title>
        {title}
        TruliDist
      </title>
      <meta name='description' content={description} />
      {/* <link
        rel='apple-touch-icon'
        sizes='76x76'
        href={'/apple-touch-icon.png'}
      />
      <link
        rel='icon'
        type='image/png'
        sizes='32x32'
        href={'/favicon-32x32.png'}
      />
      <link
        rel='icon'
        type='image/png'
        sizes='16x16'
        href={'/favicon-16x16.png'}
      /> */}
      {/* <link rel='manifest' href={'/site.webmanifest'} /> */}
      {/* <link rel='mask-icon' href={'/safari-pinned-tab.svg'} color='#1094ab' /> */}
      {/* <meta name='msapplication-TileColor' content='#1094ab' /> */}
      {/* <meta name='theme-color' content='#1094ab' /> */}
      <link
        rel='stylesheet'
        href='https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css'
        integrity='sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T'
        crossorigin='anonymous'
      />
    </Helmet>
  )
}

export default Head
