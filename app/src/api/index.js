import { URL_API } from '../config/settings'

const TYPE = {
  JSON: 'application/json',
  FORM_DATA: 'multipart/form-data'
}

const METHOD = {
  GET: 'GET',
  POST: 'POST',
  PUT: 'PUT',
  DELETE: 'DELETE'
}

export const v1 = {
  root: URL_API,

  /**
   * Fetch url
   */
  call: async (url, parameters) => {
    const finalUrl = url.indexOf(v1.root) === 0 ? url : `${v1.root}${url}`
    return await fetch(finalUrl, parameters)
  },

  /**
   * Prepare parameters
   */
  parameters: (
    accessToken,
    method = METHOD.GET,
    accept = TYPE.JSON,
    body = {},
    stringify = true
  ) => {
    const withBody = [METHOD.POST]
    const params = {
      method,
      headers: {
        Accept: accept
      }
    }
    if (accessToken) {
      params.headers = {
        ...params.headers,
        Authorization: `Bearer ${accessToken}`
        // 'Cache-Control': 'no-cache'
      }
    }
    if (withBody.indexOf(method) !== -1) {
      params.body = stringify ? JSON.stringify(body) : body

      if (method === METHOD.PUT) {
        params.headers['Content-Length'] = 0
      }
    }
    return params
  },

  /**
   * Methods
   */
  get: async (url, accessToken) => {
    const response = await v1.call(url, v1.parameters(accessToken))
    return response
  },

  getJson: async (url, accessToken) => {
    const response = await v1.get(url, accessToken)
    return await response.json()
  },

  post: async (url, body = {}, accessToken) => {
    const response = await v1.call(
      url,
      v1.parameters(accessToken, METHOD.POST, TYPE.JSON, body)
    )
    return response
  },

  postFormData: async (url, body = {}, accessToken) => {
    const params = v1.parameters(accessToken, METHOD.POST, TYPE.JSON, body)
    params.headers['Content-Type'] = 'multipart/form-data'

    const response = await v1.call(
      url,
      v1.parameters(accessToken, METHOD.POST, TYPE.JSON, body)
    )
    return response
  },

  postJson: async (url, body = {}, accessToken) => {
    try {
      const response = await v1.post(url, body, accessToken)
      return await response.json()
    } catch (error) {
      console.log(error)
    }
  },

  delete: async (url, accessToken) => {
    const response = await v1.call(
      url,
      v1.parameters(accessToken, METHOD.DELETE)
    )
    return await response.json()
  },

  put: async (url, body = {}, accessToken) => {
    const response = await v1.call(
      url,
      v1.parameters(accessToken, METHOD.PUT, TYPE.JSON, body, false)
    )
    return await response.json()
  }
}

export const updateUser = (user, accessToken) =>
  v1.postJson('/user/update', user, accessToken)

export const getUser = accessToken => v1.getJson('/user', accessToken)

/**
 * @param {string} username
 * @param {string} password
 */
export const authParameters = (username, password, { ...data }) => ({
  method: METHOD.POST,
  headers: {
    Accept: TYPE.JSON

    // @HACK 'Content-Type': 'application/json' generates a warning on Chrome on localhost. I believe this warning doesn't show on https calls
    // 'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    username,
    password,
    ...data
  })
})

export async function auth (endpoint, { username, password, ...data }) {
  const params = authParameters(username, password, { ...data })
  const response = await v1.call(endpoint, params)
  return response.json()
}
