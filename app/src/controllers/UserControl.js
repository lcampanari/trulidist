import { auth, v1 } from '../api'

const UserControl = {
  register: ({ ...data }) => {
    return auth('/register', { ...data }).catch(err =>
      console.log(`REGISTER ERROR: ${err}`)
    )
  },

  signIn: ({ username, password }) => {
    return auth('/login', { username, password })
      .then(user => {
        if (user.error) {
          return { error: user.error }
        } else {
          localStorage.setItem('token', user.token)
        }
        return { user }
      })
      .catch(err => console.log(`LOGIN ERROR: ${err}`))
  },

  signOut: () => {
    localStorage.removeItem('token')
  },

  isSignedIn: () => {
    return !!UserControl.getToken()
  },

  getToken: () => localStorage.getItem('token'),

  updateUser: user => {
    return v1.postJson('/user/update', user, UserControl.getToken())
  },

  fetch: username => {
    return v1.postJson('/user', { username })
  }
}

export default UserControl
